import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { CompressionTypes } from '@nestjs/microservices/external/kafka.interface';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.connectMicroservice({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: 'client' + Math.random(),
        brokers: ['pkc-ep9mm.us-east-2.aws.confluent.cloud:9092'],
        ssl: true,
        sasl: {
          mechanism: 'plain',
          username: process.env.CONFLUENT_API_KEY,
          password: process.env.CONFLUENT_API_SECRET,
        },
        connectionTimeout: 3000,
        requestTimeout: 25000,
        retry: {
          initialRetryTime: 100,
          retries: 8,
        },
      },
      producer: {
        retry: {
          initialRetryTime: 100,
          retries: 8,
        },
      },
      send: {
        timeout: 50000,
        compression: CompressionTypes.GZIP,
      },
    },
  });
  await app.startAllMicroservices();
  await app.listen(3001);
}
bootstrap();
