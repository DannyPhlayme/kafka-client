import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CheckoutModule } from './checkout/checkout.module';

@Module({
  imports: [ConfigModule.forRoot(), CheckoutModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
