import { Controller, Get } from '@nestjs/common';
import { CheckoutService } from './checkout.service';

@Controller('checkout')
export class CheckoutController {
  constructor(private readonly checkoutServie: CheckoutService) {}

  @Get()
  async checkout() {
    this.checkoutServie.checkout();
  }
}
