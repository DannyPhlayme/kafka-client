import { Module } from '@nestjs/common';
import { CheckoutController } from './checkout.controller';
import { CheckoutService } from './checkout.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { CompressionTypes } from '@nestjs/microservices/external/kafka.interface';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ClientsModule.register([
      {
        name: 'KAFKA_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'client' + Math.random(),
            brokers: ['pkc-ep9mm.us-east-2.aws.confluent.cloud:9092'],
            ssl: true,
            sasl: {
              mechanism: 'plain',
              username: process.env.CONFLUENT_API_KEY,
              password: process.env.CONFLUENT_API_SECRET,
            },
            connectionTimeout: 3000,
            requestTimeout: 25000,
            retry: {
              initialRetryTime: 100,
              retries: 8,
            },
          },
          producer: {
            retry: {
              initialRetryTime: 100,
              retries: 8,
            },
          },
          send: {
            timeout: 50000,
            compression: CompressionTypes.GZIP,
          },
        },
      },
    ]),
  ],
  controllers: [CheckoutController],
  providers: [CheckoutService],
})
export class CheckoutModule {}
