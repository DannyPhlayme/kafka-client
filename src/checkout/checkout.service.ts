import { Injectable, Inject, OnModuleInit } from '@nestjs/common';
import { Producer } from '@nestjs/microservices/external/kafka.interface';
import { ClientKafka } from '@nestjs/microservices';

@Injectable()
export class CheckoutService implements OnModuleInit {
  private kafkaProducer: Producer;

  constructor(
    @Inject('KAFKA_SERVICE')
    private clientKafka: ClientKafka,
  ) {}

  async onModuleInit() {
    this.kafkaProducer = await this.clientKafka.connect();
  }

  public async checkout() {
    const result = await this.kafkaProducer.send({
      topic: 'send-email',
      messages: [
        {
          key: 'payload',
          value: JSON.stringify({
            uuid: 1,
            address: 'dannyopeyemi@gmail.com',
            subject: 'Account Verification',
            body: 'You are a brave boy',
          }),
        },
      ],
    });

    console.log(result);
    // await this.kafkaProducer.disconnect();
  }
}
